<?php

/**
 * @file
 * Contains \Drupal\content_modification_log\Form\ContentModificationLogSettingsForm
 */
namespace Drupal\content_modification_log\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Url;

/**
 * Configure content_modification_log settings for this site.
 */
class ContentModificationLogSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_modification_log_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'content_modification_log.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('content_modification_log.settings');

    $form['content_modification_log_clear_log'] = [
      '#type' => 'details',
      '#title' => t('Clear content modification log'),
      '#open' => TRUE,
    ];

    $form['content_modification_log_clear_log']['clear'] = [
      '#type' => 'submit',
      '#value' => t('Clear Log Data'),
      '#submit' => ['::submitClearLog'],
    ];

    $form['content_modification_log_clear_log']['clear'] = [
      '#type' => 'link',
      '#url' => Url::fromRoute('content_modification_log.delete'),
      '#title' => t('Clear Log Data'),
      '#attributes' => [
        'class' => ['button']
      ],
    ];

    $form['content_modification_log_log_settings'] = [
      '#type' => 'details',
      '#title' => t('Content Modification Log Settings'),
      '#open' => TRUE,
    ];

    $form['content_modification_log_log_settings']['content_modification_log_rowcount'] = [
      '#type' => 'number',
      '#title' => $this->t('Rows per page'),
      '#placeholder' => $this->t('Link label'),
      '#size' => 4,
      '#min' => 1,
      '#max' => 100,
      '#description' => $this->t('Set the number of rows per page on the Content Modification Log table.'),
      '#default_value' => (($config->get('acl_rowcount')) ?: 50),
      '#required' => TRUE,
    ];

    $form['content_modification_log_log_settings']['content_modification_log_show_tab'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable the Content modifications tab.'),
      '#description' => $this->t('Show the Content modifications tab on the Content overview page.'),
      '#default_value' => (($config->get('show_tab')) ?: false),
    ];

    $form['content_modification_log_csv_settings'] = [
      '#type' => 'details',
      '#title' => t('CSV File Settings'),
      '#open' => TRUE,
    ];

    $form['content_modification_log_csv_settings']['content_modification_log_csv_filename'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSV Filename'),
      '#description' => $this->t('Set the name of the CSV created when "Export to CSV" is clicked.<br><b>Note:</b> Tokens can be used to create the filename.'),
      '#maxlenth' => 128,
      '#required' => TRUE,
      '#default_value' => (($config->get('acl_csv_filename')) ?: 'content-log.csv'),
    ];

    // Add the token tree UI.
    $form['content_modification_log_csv_settings']['token_tree'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['user'],
      '#show_restricted' => TRUE,
      '#weight' => 90,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = \Drupal::service('config.factory')->getEditable('content_modification_log.settings');
    $config
      ->set('acl_rowcount', $form_state->getValue('content_modification_log_rowcount'))
      ->set('acl_csv_filename', $form_state->getValue('content_modification_log_csv_filename'))
      ->set('show_tab', $form_state->getValue('content_modification_log_show_tab'))
      ->save();

    Cache::invalidateTags(array('config:content_modification_log.settings'));

  }
}
