<?php

namespace Drupal\content_modification_log\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a confirmation form to confirm deletion of content modification log data.
 */
class ContentModificationLogConfirmDeleteForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    \Drupal::messenger()->addStatus(t('The Content Modification Log has been cleared.'));

    \Drupal::database()->truncate('content_modification_log')->execute();

    $form_state->setRedirect('content_modification_log.settings');

  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "content_modification_log_confirm_delete_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('content_modification_log.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Delete all Content Modification Log data?');
  }

}
