<?php

namespace Drupal\content_modification_log\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements a global content modification log.
 *
 * @Block(
 *   id = "content_modification_log",
 *   admin_label = @Translation("Content Modification Log"),
 * )
 */
class ContentModificationLogBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['content_modification_log_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Content Modification Log Configuration Field'),
      '#size' => 60,
      '#description' => $this->t('A field to store a configuration value.'),
      '#default_value' => (empty($config['content_modification_log_field']) ? '' : $config['content_modification_log_field']),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();

    $config_value = (empty($config['content_modification_log_field']) ? '' : $config['content_modification_log_field']);


    // Build the block.
    $build = [];

    $block = [
      '#theme' => 'content_modification_log',
      '#attributes' => [
        'class' => ['content-modification-log'],
        'id' => $this->getBaseId(),
      ],
      '#content' => [
        'cid' => $config_value,
      ],
      '#cache' => [
        'tags' => ['config:content_modification_log.settings']
      ],
      '#attached' => [
        'library' => 'content_modification_log/content_modification_log.functions',
        'drupalSettings' => [
          'json_url' => $config_value,
        ],
      ]
    ];

    $build['content-modification-log-block'] = $block;

    return $build;

  }


  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['content_modification_log_field'] = $values['content_modification_log_field'];
  }

}
